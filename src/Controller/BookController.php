<?php

namespace App\Controller;

use App\Entity\Livre;
use App\Form\LivreType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BookController extends AbstractController
{
    #[Route('/', name: 'app_book')]
    public function index(ManagerRegistry $doctrine): Response
    {

        $livres = $doctrine->getRepository(Livre::class)->findAll();

        

        return $this->render('book/index.html.twig', [
            'livres' => $livres,
        ]);
    }

    #[Route('/addBook', name: 'addBook')]
    public function addBook(Request $request, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();
       

        $livre = new Livre();

        $form = $this->createForm(LivreType::class, $livre);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            
            // $livre = $form->getData();
            
            $entityManager->persist($livre);
            $entityManager->flush();


            return $this->redirectToRoute('app_book');
        }

        return $this->renderForm('book/newBook.html.twig', [
            'form' => $form,
        ]);

    }
}
